"use script";

console.log("PRACTICE");

console.log("TASK 3");


/* Напишіть деструктуруюче присвоєння, яке:

властивість name присвоїть в змінну ім'я
властивість years присвоїть в змінну вік
властивість isAdmin присвоює в змінну isAdmin false, якщо такої властивості немає в об'єкті

Виведіть змінні на екран. */

const user1 = {
    name: "John",
    years: 30
};

let { name, years: age, isAdmin } = user1;
if ("isAdmin" in user1) {
    isAdmin = true;
} else {
    isAdmin = false;
}

console.log(name, age, isAdmin);
