"use script";

console.log("PRACTICE");

console.log("TASK 6");

/* Даний об'єкт employee. Додайте до нього властивості age і salary, не змінюючи початковий об'єкт 
(має бути створено новий об'єкт, який включатиме всі необхідні властивості). Виведіть новий об'єкт у консоль. */

const employee = {
    name: 'Vitalii',
    surname: 'Klichko',
}

// const updatedObject = {};

let { name: newName, surname } = employee;

// console.log(newName);
// console.log(surname);

const updatedObject =
{
    // newName: name, 
    // newSurname: surname,
};

updatedObject.newName = 'Vitalii';
updatedObject.surname = 'Klichko';
updatedObject.age = 20;
updatedObject.salary = 1000;

console.log(updatedObject);